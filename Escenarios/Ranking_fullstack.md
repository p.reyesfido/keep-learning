# Ranking Services

Un jugador de juegos de arcade quiere subir a la cima de la tabla de clasificación y rastrear su clasificación. El juego usa **Dense Ranking** , por lo que su tabla de clasificación funciona así:

- El jugador con la puntuación más alta es el número clasificado **1** en la tabla de clasificación.
- Los jugadores que tienen puntuaciones iguales reciben el mismo número de clasificación, y los siguientes jugadores reciben el número de clasificación inmediatamente siguiente.
#### Ejemplo
*ranked = [100,90,90,80]*
*player = [70,80,105]*


Los jugadores clasificados tendrán rangos **1**, **2**, **2**  y **3**, respectivamente. Si las puntuaciones del jugador son **70**, **80**  y **105**, sus clasificaciones después de cada juego son **4^to^**, **3^er^** y **1^er^**. Regreso **[4,3,1]**

#### Función descriptiva

Desarrolla un **Microservicio** para realizar los Ranking's del videojuego.
RankingBoard tiene los siguientes parámetros:
- *int clasificado [n]: las puntuaciones de la tabla de clasificación*
- *int player [m]: las puntuaciones del jugador*

#### Formato de datos

La primera línea contiene un número entero ***N***, el número de jugadores en la clasificación.
La siguiente línea contiene ***N*** enteros separados por espacios ***ranked[i]*** , la tabla de clasificación puntúa en orden decreciente.
La siguiente línea contiene un número entero ***M***, el número de juegos que juega el jugador.
La última línea contiene ***M*** enteros separados por espacios ***player[j]*** , el juego puntúa.

#### Restricciones

- ***1 ≤ n ≤ 2 x 10^5^***
- ***1 ≤ m ≤ 2 x 10^5^***
- ***0 ≤ ranked[i] ≤ 10^9^*** por ***0 ≤ i < n***
- ***0 ≤ ranked[j] ≤ 10^9^*** por ***0 ≤ j < n***
- La tabla de clasificaión existente, ***ranked***, esta en orden descendiente.
- Las puntuaciones del jugador, ***player***, están en orden ascendente .

#### Subtarea

Para ***60%*** de la puntuación maxima:
- ***1 ≤ n ≤ 200***
- ***1 ≤ m ≤ 200***

#### Ejemplo entrada

Table Ranked
| 100 | 100 | 50 | 40 | 40 | 20 | 10 |
| --- | --- | --- | --- | --- | --- |  --- |

Table Player
| 5 | 25 | 50 | 160 |
| --- | --- | --- | --- | 
|R^[R=result on ranking]= 6| R = 4| R = 2| R = 1

#### FrontEnd 

Se debe tener un formulario para poder cargar la información de los jugadores con sus resultados y un modulo separado para ver la tabla de rankings globales en tiempo real

### Consideraciones 

Los desarrollos deben ser en **NodeJS** y las actualizaciones de información deben ser mediante solicitudes REST
- Los **servicios** deben contar con los verbos CRUD en la base de datos
- Cada jugador debe guardar su ultimo **maximo** puntaje
- La base de datos debe ser **PostgreSQL**
- En el FrontEnd se puede utilizar alguno de estos frameworks:
    - Angular
    - Vue
    - React

